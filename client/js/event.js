//currently abandoned
function fire(event){
    /*
    //this is small bullet, other bullet will be added later
    for(var i=0;i<bullet.small_bullet.num;++i){
        bullet.small_bullet.stack.push({
            //the position is the begin position
            //example: to calc the position.x current you need to add the cos(direction)*journey
            position    :{
                x       :car.position.x,
                y       :car.position.y,
            },
            direction   :car.direction + bullet.small_bullet.precision * Math.random(),
            journey     :0,
        })
    }
    */
}

function mouse_down(event){
    if(camera.speed_up.fuel>1.1)
        log_alert("Hey! dont cheat")
    if(camera.speed_up.fuel>camera.speed_up.speed_up_min)
        car.b_speed_up = true;
}

function mouse_up(event){
    car.b_speed_up=false;
}

function mouse_move(event) {
    //change the mouse properity
    mouse.position.x                     = event.clientX - main.canvas.width / 2;
    mouse.position.y                     = event.clientY - main.canvas.height / 2;
    mouse.distance_square       = mouse.position.x * mouse.position.x + mouse.position.y * mouse.position.y;

    //the math.atan2 is fucking useful that i dont need to make the result range from 0 to 2PI
    mouse.direction             = Math.atan2(mouse.position.y, mouse.position.x);

    //calc the speed
    camera.speed_circle.radius  = Math.min(main.canvas.height / 5, main.canvas.width / 8);
    var t                       = Math.min(1, mouse.distance_square / (camera.speed_circle.radius * camera.speed_circle.radius));
    car.speed                   = car.speed_max * t + car.speed_min * (1 - t);

    //change the color of the speed circle 
    if (mouse.distance_square < camera.speed_circle.radius * camera.speed_circle.radius)
        camera.speed_circle.color = "#FF00007F";  //red 
    else
        camera.speed_circle.color = "#0000FF7F";  //blue
}
