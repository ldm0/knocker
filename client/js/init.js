function init() {
    //init the global value
    init_var();

    //bind the event  
    init_bind();

    //the communicate module
    //var communicate = new Worker("communicate.js");
}

function init_var(){
    main=new main();
    map=new map();
    car=new car();
    mouse=new mouse();
    camera=new camera();
}

function init_bind(){
    main.canvas.addEventListener("mousedown",mouse_down);
    main.canvas.addEventListener("mouseup",mouse_up);
    main.canvas.addEventListener("mousemove",mouse_move);
}