function move(){
    move_car();
    move_camera();
    move_tail();
    move_speed_up();
    //move_bullet();
}

function move_speed_up(){
    if(camera.speed_up.fuel<=0)
        car.b_speed_up=false;
    if(car.b_speed_up)
        camera.speed_up.fuel-=0.01;
    else
        camera.speed_up.fuel=Math.min(1,camera.speed_up.fuel+0.001);
}

function move_car(){
    //the move includes the rotate and move

    //car rotate
    {
        //the mouse direction influence the car direction
        //the car have inertia, so it cannot turn too fast, a little f is essential
        var turn_angle = 0;

        /*
            1
        |------|
      8 |      | 2
        |------|
            4
        */
        var up=1;
        var right=2;
        var down=4;
        var left=8;

        var state = 0;
        if(camera.position.x<0)
            state^=left;
        if(camera.position.y<0)
            state^=up;
        if(camera.position.x>map.width)
            state^=right;
        if(camera.position.y>map.height)
            state^=down;

        //out of the wall
        if(state){
            if ((state ^ up) == 0 && car.direction < 0)
                turn_angle = -2 * car.direction;
            if ((state ^ down) == 0 && car.direction > 0)
                turn_angle = -2 * car.direction;

            if ((state ^ right) == 0) {
                if (car.direction >= 0 && car.direction <= Math.PI / 2)
                    turn_angle = 2 * (Math.PI / 2 - car.direction);
                if (car.direction <= 0 && car.direction > -Math.PI / 2)
                    turn_angle = 2 * (-Math.PI / 2 - car.direction);
            } 
            if ((state ^ left) == 0) { 
                if(car.direction > Math.PI / 2)
                    turn_angle = 2 * (Math.PI / 2 - car.direction);
                if(car.direction < -Math.PI / 2)
                    turn_angle = 2 * (-Math.PI / 2 - car.direction);
            } 

            //the corner pending
            if ((state ^ right ^ up) == 0)
                car.direction = (car.direction + 2 * Math.PI) % (Math.PI / 2) + Math.PI / 2;
            if ((state ^ right ^ down) == 0)
                car.direction = (car.direction + 2 * Math.PI) % (Math.PI / 2) - Math.PI;
            if ((state ^ left ^ up) == 0)
                car.direction = (car.direction + 2 * Math.PI) % (Math.PI / 2);
            if ((state ^ left ^ down) == 0)
                car.direction = (car.direction + 2 * Math.PI) % (Math.PI / 2) - Math.PI / 2;

            camera.blood_bar.blood_value    *=0.9;
            camera.blood_bar.color_blood    =blood_color(camera.blood_bar.blood_value);
        } else {
            turn_angle      = mouse.direction - car.direction;
            var max_angle   = car.turn_angle_max / ((car.speed+10) * main.fps);

            if (turn_angle > Math.PI)
                turn_angle  = -2 * Math.PI + turn_angle;
            else if (turn_angle < -Math.PI)
                turn_angle  = 2 * Math.PI + turn_angle;

            if (turn_angle > max_angle)
                turn_angle  = max_angle;
            else if (turn_angle < -max_angle)
                turn_angle  = -max_angle;
        }

        //mouse direction influence the car direction
        car.direction       += turn_angle;
        car.direction       = ((car.direction + 2 * Math.PI) % (2 * Math.PI)+Math.PI)%(2*Math.PI)-Math.PI; // Math.PI;
    }

    //car move
    {
        var speed_actual    = (car.speed + car.b_speed_up * camera.speed_up.speed);
        car.position.x      += Math.cos(car.direction) * speed_actual; 
        car.position.y      += Math.sin(car.direction) * speed_actual; 
    }
}

function move_camera(){
    //actually the camera's move is the same as the car's
    //camera move
    camera.position.x       = car.position.x - main.canvas.width / 2;
    camera.position.y       = car.position.y - main.canvas.height / 2;
}

function move_tail(){
    //we call it "move" but actually we only sample a position of car 
    //and cover the last position sampled
    //which looks like the tail "moved"
    //sample
    car.tail.data_queue[car.tail.head].x = car.position.x;
    car.tail.data_queue[car.tail.head].y = car.position.y;

    car.tail.head = (car.tail.head + 1) % car.tail.sample_num;
}

function move_bullet(){
    //the bullet move towards its direction with the speed of the bullet.speed

}
    
